package files;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import database.LibraryDatabaseManager;
import objectstructures.Book;
import objectstructures.BookLibrary;
import objectstructures.Library;

public class InputProcessor {

	private FileType fileProcessor;
	private String filePath;

	public void setFileProcessor(String filePath) throws WrongFileTypeException {
		String extension = this.getFileType(filePath);
		this.fileProcessor = FileType.getInstance(extension);
		this.filePath = filePath;
	}

	public String readFileContent(String filePath) throws IOException {
		String pathName = "C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\160h\\LibrariesFiles\\" + filePath;
		return Files.readString(Paths.get(pathName), StandardCharsets.UTF_8);
	}

	public String getFileType(String path) {
		StringBuffer fileExt = new StringBuffer();

		for (int i = path.length() - 1; i > 0; i--) {
			if (path.charAt(i) == '.')
				break;
			fileExt.append(path.charAt(i));
		}
		return fileExt.reverse().toString();
	}

	public List<Library> processLibrariesFile() throws IOException {

		return this.fileProcessor.getLibraries(this.readFileContent(this.filePath));
	}

	public List<Book> processBooksFile() throws IOException {

		return this.fileProcessor.getBooks(this.readFileContent(this.filePath));
	}

	public List<BookLibrary> processLibrariesBooksFile() throws Exception {

		List<BookLibrary> librariesBooks = this.fileProcessor.getBooksLibraries(this.readFileContent(this.filePath));
		LibraryDatabaseManager libraryDatabaseManager = new LibraryDatabaseManager();
		libraryDatabaseManager.setBooksLibrariesIds(librariesBooks);
		return librariesBooks;
	}

}
