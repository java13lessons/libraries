package files;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import database.DatabaseManager;
import database.LibraryDatabaseManager;
import objectstructures.Book;
import objectstructures.BookLibrary;
import objectstructures.Library;

public class CSV extends FileType {

	public static final String SEPARATOR = ",";

	private String[] getColumnNames(String headerLine) {
		StringTokenizer columnNamesTok = new StringTokenizer(headerLine, SEPARATOR);
		String[] columnNamesArr = new String[columnNamesTok.countTokens()];
		int currentColumnNumber = -1;
		while (columnNamesTok.hasMoreTokens()) {
			currentColumnNumber++;
			String columnName = columnNamesTok.nextToken();
			columnNamesArr[currentColumnNumber] = columnName;
		}
		return columnNamesArr;
	}

	@Override
	public List<Library> getLibraries(String fileContent) {
		List<Library> libraries = new ArrayList<Library>();
		String[] lines = fileContent.split(System.lineSeparator());

		if (lines.length < 1)
			return null;

		String[] columnNames = this.getColumnNames(lines[0]);
		for (int i = 1; i < lines.length; i++) {
			Library library = new Library();
			libraries.add(library);

			String line = lines[i];
			StringTokenizer lineStrTok = new StringTokenizer(line, SEPARATOR);
			int currentColumnNumber = -1;
			while (lineStrTok.hasMoreTokens()) {
				currentColumnNumber++;
				String columnValue = lineStrTok.nextToken();
				switch (columnNames[currentColumnNumber]) {
				case "Name":
					library.setName(columnValue);
					break;
				case "ExternalId":
					library.setExternalId(columnValue);
					break;
				}
			}
		}

		return libraries;
	}

	@Override
	public List<Book> getBooks(String fileContent) {
		List<Book> books = new ArrayList<Book>();
		String[] lines = fileContent.split(System.lineSeparator());

		if (lines.length < 1)
			return null;

		String[] columnNames = this.getColumnNames(lines[0]);
		for (int i = 1; i < lines.length; i++) {
			Book book = new Book();
			books.add(book);

			String line = lines[i];
			StringTokenizer lineStrTok = new StringTokenizer(line, SEPARATOR);
			int currentColumnNumber = -1;
			while (lineStrTok.hasMoreTokens()) {
				currentColumnNumber++;
				String columnValue = lineStrTok.nextToken();
				switch (columnNames[currentColumnNumber]) {
				case "Name":
					book.setName(columnValue);
					break;
				case "ExternalId":
					book.setExternalId(columnValue);
					break;
				case "Author":
					book.setAuthor(columnValue);
					break;
				case "Date_Published":
					book.setDatePublished(columnValue);
					break;
				}
			}
		}

		return books;
	}

	@Override
	public void exportLibraries(String fileName) {

		try {
			LibraryDatabaseManager libraryDatabaseManager = new LibraryDatabaseManager();
			Collection<Library> libraries = libraryDatabaseManager.getLibraries().values();
			StringBuffer csvFileContent = new StringBuffer("Name,ExternalId" + System.lineSeparator());
			Iterator<Library> iterator = libraries.iterator();
			while (iterator.hasNext()) {
				Library library = iterator.next();
				csvFileContent.append(library.getName() + "," + library.getExternalId() + System.lineSeparator());
			}

			this.writeToFile(fileName, csvFileContent.toString());
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}

	}

	@Override
	public List<BookLibrary> getBooksLibraries(String content) {
		List<BookLibrary> booksLibraries = new ArrayList<BookLibrary>();
		String[] lines = content.split(System.lineSeparator());

		if (lines.length < 1)
			return null;

		String[] columnNames = this.getColumnNames(lines[0]);
		for (int i = 1; i < lines.length; i++) {
			BookLibrary bookLibrary = new BookLibrary();
			booksLibraries.add(bookLibrary);

			String line = lines[i];
			StringTokenizer lineStrTok = new StringTokenizer(line, SEPARATOR);
			int currentColumnNumber = -1;
			while (lineStrTok.hasMoreTokens()) {
				currentColumnNumber++;
				String columnValue = lineStrTok.nextToken();
				switch (columnNames[currentColumnNumber]) {
				case "LibraryId":
					bookLibrary.setLibraryExternalId(columnValue);
					break;
				case "BookId":
					bookLibrary.setBookExternalId(columnValue);
					break;
				case "EntryId":
					bookLibrary.setExternalId(columnValue);
					break;
				}
			}
		}
		return booksLibraries;
	}

}
