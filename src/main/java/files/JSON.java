package files;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONWriter;

import database.DatabaseManager;
import database.LibraryDatabaseManager;
import objectstructures.Book;
import objectstructures.BookLibrary;
import objectstructures.Library;

public class JSON extends FileType {

	@Override
	public List<Book> getBooks(String fileContent) {
		List<Book> books = new ArrayList<Book>();
		JSONObject fileObject = new JSONObject(fileContent);
		JSONArray booksJSON = fileObject.getJSONArray("Books");
		Iterator<Object> iterator = booksJSON.iterator();
		while (iterator.hasNext()) {
			JSONObject bookJSON = (JSONObject) iterator.next();
			Book book = new Book();
			books.add(book);

			book.setName(bookJSON.getString("Name"));
			book.setExternalId(bookJSON.getString("ExternalId"));
			book.setAuthor(bookJSON.getString("Author"));
			book.setDatePublished(bookJSON.getString("Date_Published"));

		}
		return books;
	}

	@Override
	public List<Library> getLibraries(String fileContent) {
		List<Library> libraries = new ArrayList<Library>();
		JSONObject fileObject = new JSONObject(fileContent);
		JSONArray librariesJSON = fileObject.getJSONArray("Libraries");
		Iterator<Object> iterator = librariesJSON.iterator();
		while (iterator.hasNext()) {
			JSONObject libraryJSON = (JSONObject) iterator.next();
			Library library = new Library();
			libraries.add(library);

			library.setName(libraryJSON.getString("Name"));
			library.setExternalId(libraryJSON.getString("ExternalId"));
		}
		return libraries;
	}

	@Override
	public void exportLibraries(String fileName) {
		try {
			LibraryDatabaseManager libraryDatabaseManager = new LibraryDatabaseManager();
			Collection<Library> libraries = libraryDatabaseManager.getLibraries().values();

			StringWriter stringWriter = new StringWriter();
			JSONWriter jsonWriter = new JSONWriter(stringWriter);
			jsonWriter.object().key("Libraries").value(libraries).endObject();

			this.writeToFile(fileName, stringWriter.toString());
			stringWriter.close();

		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}

	}

	@Override
	public List<BookLibrary> getBooksLibraries(String content) {
		List<BookLibrary> librariesBooks = new ArrayList<BookLibrary>();
		JSONObject fileObject = new JSONObject(content);
		JSONArray librariesBooksJSON = fileObject.getJSONArray("BooksLibraries");
		Iterator<Object> iterator = librariesBooksJSON.iterator();
		while (iterator.hasNext()) {
			JSONObject libraryBookJSON = (JSONObject) iterator.next();
			BookLibrary libraryBook = new BookLibrary();
			libraryBook.setBookExternalId(libraryBookJSON.getString("BookId"));
			libraryBook.setLibraryExternalId(libraryBookJSON.getString("LibraryId"));
			libraryBook.setExternalId(libraryBookJSON.getString("EntryId"));

			librariesBooks.add(libraryBook);
		}

		return librariesBooks;
	}

}
