package files;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import objectstructures.Book;
import objectstructures.BookLibrary;
import objectstructures.Library;

public abstract class FileType {

	////Factory method approach
	public static FileType getInstance(String fileExtension) throws WrongFileTypeException {

		switch (fileExtension.toLowerCase()) {
		case "csv":
			return new CSV();
		case "json":
			return new JSON();
		case "xml":
			return new XML();
		default:
			throw new WrongFileTypeException(fileExtension);
		}

	}

	public abstract List<Library> getLibraries(String fileContent);

	public abstract List<Book> getBooks(String fileContent);

	public abstract void exportLibraries(String fileName);

	public abstract List<BookLibrary> getBooksLibraries(String content);

	protected void writeToFile(String fileName, String content) throws IOException {
		FileWriter writer = new FileWriter(fileName, StandardCharsets.UTF_8);
		writer.write(content);
		writer.close();
	}

}
