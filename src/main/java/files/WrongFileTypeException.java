package files;

public class WrongFileTypeException extends Exception {

	private String fileExtension;
	/**
	 * 
	 */
	private static final long serialVersionUID = 4997556883344334189L;

	public WrongFileTypeException(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFileExtension() {
		return fileExtension;
	}

}
