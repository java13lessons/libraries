package files;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import database.DatabaseManager;
import database.LibraryDatabaseManager;
import objectstructures.Book;
import objectstructures.BookLibrary;
import objectstructures.Library;

public class XML extends FileType {

	@Override
	public List<Book> getBooks(String fileContent) {
		List<Book> books = new ArrayList<Book>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new InputSource(new StringReader(fileContent)));
			Element booksRoot = xmlDocument.getDocumentElement();
			NodeList booksElements = booksRoot.getChildNodes();

			for (int i = 0; i < booksElements.getLength(); i++) {
				Book book = new Book();
				Node bookElement = booksElements.item(i);
				NodeList bookParamaters = bookElement.getChildNodes();
				if (bookParamaters.getLength() == 0)
					continue;

				books.add(book);
				for (int j = 0; j < bookParamaters.getLength(); j++) {
					Node parameter = bookParamaters.item(j);
					switch (parameter.getNodeName()) {
					case "Name":
						book.setName(parameter.getTextContent());
						break;
					case "ExternalId":
						book.setExternalId(parameter.getTextContent());
						break;
					case "Author":
						book.setAuthor(parameter.getTextContent());
						break;
					case "Date_Published":
						book.setDatePublished(parameter.getTextContent());
						break;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return books;
	}

	@Override
	public List<Library> getLibraries(String fileContent) {
		List<Library> libraries = new ArrayList<Library>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new InputSource(new StringReader(fileContent)));
			Element librariesRoot = xmlDocument.getDocumentElement();
			NodeList librariesElements = librariesRoot.getChildNodes();

			for (int i = 0; i < librariesElements.getLength(); i++) {
				Library library = new Library();
				Node libraryElement = librariesElements.item(i);
				NodeList libraryParamaters = libraryElement.getChildNodes();
				if (libraryParamaters.getLength() == 0)
					continue;

				libraries.add(library);
				for (int j = 0; j < libraryParamaters.getLength(); j++) {
					Node parameter = libraryParamaters.item(j);
					switch (parameter.getNodeName()) {
					case "Name":
						library.setName(parameter.getTextContent());
						break;
					case "ExternalId":
						library.setExternalId(parameter.getTextContent());
						break;
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return libraries;
	}

	@Override
	public void exportLibraries(String fileName) {
		try {
			LibraryDatabaseManager libraryDatabaseManager = new LibraryDatabaseManager();
			Collection<Library> libraries = libraryDatabaseManager.getLibraries().values();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element librariesElement = document.createElement("libraries");
			document.appendChild(librariesElement);

			Iterator<Library> iterator = libraries.iterator();
			while (iterator.hasNext()) {
				Library library = iterator.next();
				Element libraryElement = document.createElement("library");
				Element nameElement = document.createElement("name");
				nameElement.setTextContent(library.getName());

				Element idElement = document.createElement("id");
				idElement.setTextContent(String.valueOf(library.getId()));

				libraryElement.appendChild(idElement);
				libraryElement.appendChild(nameElement);

				if (library.getExternalId() != null && !library.getExternalId().equals("")) {
					Element externalIdElement = document.createElement("externalId");
					externalIdElement.setTextContent(library.getExternalId());
					libraryElement.appendChild(externalIdElement);
				}

				librariesElement.appendChild(libraryElement);
			}
			DOMSource domSource = new DOMSource(document);
			StringWriter strWriter = new StringWriter();
			StreamResult result = new StreamResult(strWriter);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(domSource, result);

			this.writeToFile(fileName, strWriter.toString());

		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}

	}

	@Override
	public List<BookLibrary> getBooksLibraries(String content) {
		List<BookLibrary> booksLibraries = new ArrayList<BookLibrary>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new InputSource(new StringReader(content)));
			Element booksLibrariesRoot = xmlDocument.getDocumentElement();
			NodeList booksLibrariesElements = booksLibrariesRoot.getChildNodes();

			for (int i = 0; i < booksLibrariesElements.getLength(); i++) {
				BookLibrary bookLibrary = new BookLibrary();
				Node bookLibraryElement = booksLibrariesElements.item(i);
				NodeList bookLibraryParamaters = bookLibraryElement.getChildNodes();
				if (bookLibraryParamaters.getLength() == 0)
					continue;

				booksLibraries.add(bookLibrary);
				for (int j = 0; j < bookLibraryParamaters.getLength(); j++) {
					Node parameter = bookLibraryParamaters.item(j);
					switch (parameter.getNodeName()) {
					case "BookId":
						bookLibrary.setBookExternalId(parameter.getTextContent());
						break;
					case "LibraryId":
						bookLibrary.setLibraryExternalId(parameter.getTextContent());
						break;
					case "EntryId":
						bookLibrary.setExternalId(parameter.getTextContent());
						break;
					}
				}

			}

		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
			e.printStackTrace();
		}

		return booksLibraries;
	}

}
