package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.BooksDatabaseManager;
import database.DatabaseManager;

/**
 * Servlet implementation class Books
 */
@WebServlet("/Books")
public class Books extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		try {

			DatabaseManager bookManager = new BooksDatabaseManager();

			if (request.getParameter("insertNewBook") != null)
				bookManager.addNewEntry(request, response);
			else if (request.getParameter("deleteBookId") != null && request.getParameter("deleteBookId") != "")
				bookManager.deleteEntry(Integer.parseInt(request.getParameter("deleteBookId")));
			else if (request.getParameter("bookChanged") != null && request.getParameter("bookChanged") != "")
				bookManager.changeEntry(request);

			bookManager.setAllEntries(request);
			DatabaseManager.closeConnection();
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}
		RequestDispatcher view = request.getRequestDispatcher("Books.jsp");
		view.forward(request, response);
	}

}
