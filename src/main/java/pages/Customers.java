package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.CustomersDatabaseManager;
import database.DatabaseManager;

/**
 * Servlet implementation class Customers
 */
@WebServlet("/Customers")
public class Customers extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Customers() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		try {

			DatabaseManager customersDbManager = new CustomersDatabaseManager();
			if (request.getParameter("insertNewCustomer") != null)
				customersDbManager.addNewEntry(request, response);
			else if (request.getParameter("deleteCustomerId") != null && request.getParameter("deleteCustomerId") != "")
				customersDbManager.deleteEntry(Integer.parseInt(request.getParameter("deleteCustomerId")));
			else if (request.getParameter("customerRenamed") != null && request.getParameter("customerRenamed") != "")
				customersDbManager.changeEntry(request);

			customersDbManager.setAllEntries(request);
			DatabaseManager.closeConnection();

		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}
		RequestDispatcher view = request.getRequestDispatcher("Customers.jsp");
		view.forward(request, response);

	}

}
