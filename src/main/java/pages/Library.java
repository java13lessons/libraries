package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.BooksDatabaseManager;
import database.CustomersDatabaseManager;
import database.DatabaseManager;
import database.LibraryDatabaseManager;

/**
 * Servlet implementation class Library
 */
@WebServlet("/Library")
public class Library extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Library() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	private int getLibraryIdFromQuery(String query) {

		String[] queries = query.split("&");
		for (int i = 0; i < queries.length; i++) {

			String[] queryParamater = queries[i].split("=");
			if (queryParamater[0].equals("libraryId"))
				return Integer.parseInt(queryParamater[1]);
		}

		return 0;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		try {
			int libraryId = this.getLibraryIdFromQuery(request.getQueryString());
			BooksDatabaseManager booksDBManager = new BooksDatabaseManager();
			LibraryDatabaseManager libraryDbManager = new LibraryDatabaseManager();
			CustomersDatabaseManager customerDbManager = new CustomersDatabaseManager();

			if (request.getParameter("bookToAdd") != null && !request.getParameter("bookToAdd").equals(""))
				libraryDbManager.addBookToLibrary(Integer.parseInt(request.getParameter("bookToAdd")), libraryId);
			else if (request.getParameter("entryToDelete") != null && !request.getParameter("entryToDelete").equals(""))
				libraryDbManager.deleteBookLibraryEntry(Integer.parseInt(request.getParameter("entryToDelete")));
			else if (request.getParameter("borrowBookButton") != null
					&& request.getParameter("borrowBookButton").equals("on"))
				libraryDbManager.borrowBook(request);
			else if (request.getParameter("returnBookId") != null && !request.getParameter("returnBookId").equals(""))
				libraryDbManager.returnBook(Integer.parseInt(request.getParameter("returnBookId")));

			booksDBManager.setAllEntries(request);
			libraryDbManager.setAllBooksForLibrary(libraryId, request);
			libraryDbManager.setBooksNotBorrowed(libraryId, request);
			libraryDbManager.setBooksBorrowed(libraryId, request);
			customerDbManager.setAllEntries(request);

			DatabaseManager.closeConnection();

		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}

		RequestDispatcher view = request.getRequestDispatcher("Library.jsp");
		view.forward(request, response);
	}

}
