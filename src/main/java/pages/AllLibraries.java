package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;
import database.LibraryDatabaseManager;
import files.FileType;

/**
 * Servlet implementation class AllLibraries
 */
@WebServlet("/AllLibraries")
public class AllLibraries extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AllLibraries() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		try {

			DatabaseManager libraryDbManager = new LibraryDatabaseManager();
			if (request.getParameter("insertNewLibrary") != null)
				libraryDbManager.addNewEntry(request, response);
			else if (request.getParameter("deleteLibraryId") != null && request.getParameter("deleteLibraryId") != "")
				libraryDbManager.deleteEntry(Integer.parseInt(request.getParameter("deleteLibraryId")));
			else if (request.getParameter("libraryRenamed") != null && request.getParameter("libraryRenamed") != "")
				libraryDbManager.changeEntry(request);
			else if (request.getParameter("export") != null && request.getParameter("exportType") != null
					&& !request.getParameter("exportType").equals("")) {
				FileType fileType = FileType.getInstance(request.getParameter("exportType"));
				fileType.exportLibraries("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\160h\\LibrariesFiles\\"
						+ request.getParameter("fileName") + "." + request.getParameter("exportType").toLowerCase());
			}

			libraryDbManager.setAllEntries(request);

		} catch (Exception e) {
			e.printStackTrace();
			DatabaseManager.addLogMessage(e);
		}

		try {
			DatabaseManager.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher view = request.getRequestDispatcher("AllLibraries.jsp");
		view.forward(request, response);
	}

}
