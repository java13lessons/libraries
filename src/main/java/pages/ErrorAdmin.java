package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;
import database.ErrorAdminDatabaseManager;

/**
 * Servlet implementation class ErrorAdmin
 */
@WebServlet("/ErrorAdmin")
public class ErrorAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ErrorAdmin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ErrorAdminDatabaseManager errorAdminDatabaseManager = new ErrorAdminDatabaseManager();
			request.setAttribute("errors", errorAdminDatabaseManager.getLogEntries());
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}
		RequestDispatcher view = request.getRequestDispatcher("ErrorAdmin.jsp");
		view.forward(request, response);
	}

}
