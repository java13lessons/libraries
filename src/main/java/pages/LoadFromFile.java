package pages;

import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.BooksDatabaseManager;
import database.DatabaseManager;
import database.LibraryDatabaseManager;
import files.InputProcessor;
import files.WrongFileTypeException;
import objectstructures.Book;
import objectstructures.BookLibrary;
import objectstructures.Library;

/**
 * Servlet implementation class LoadFromFile
 */
@WebServlet("/LoadFromFile")
public class LoadFromFile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static List<String> errors, successes;

	public static void addExceptionMessage(String message) {
		errors.add(message);
	}

	public static void addSuccessMessage(String message) {
		successes.add(message);
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadFromFile() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	private void addAllBooks(HttpServletRequest request) {

		LoadFromFile.errors = new ArrayList<String>();
		LoadFromFile.successes = new ArrayList<String>();
		try {
			int position = 0;
			BooksDatabaseManager booksDatabaseManager = new BooksDatabaseManager();
			while (true) {
				position++;
				if (request.getParameter("deleteRowBook[" + position + "]") != null
						&& request.getParameter("deleteRowBook[" + position + "]").equals("X"))
					continue;
				try {
					String bookName = request.getParameter("bookName[" + position + "]");
					if (bookName == null)
						break;
					String externalId = request.getParameter("externalIdBook[" + position + "]");
					String author = request.getParameter("author[" + position + "]");
					String datePublished = request.getParameter("datePublished[" + position + "]");

					booksDatabaseManager.addNewEntry(bookName, author, datePublished, externalId);
					addSuccessMessage("Book " + bookName + " added!");
				} catch (SQLIntegrityConstraintViolationException e) {
					LoadFromFile.addExceptionMessage(e.getMessage());
					DatabaseManager.addLogMessage(e);
				}
			}
			DatabaseManager.closeConnection();
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}
		if (LoadFromFile.errors.size() > 0)
			request.setAttribute("exceptions", LoadFromFile.errors);
		if (LoadFromFile.successes.size() > 0)
			request.setAttribute("successes", LoadFromFile.successes);

	}

	private void addAllLibraries(HttpServletRequest request) {

		try {
			int position = 0;
			LibraryDatabaseManager libraryDatabaseManager = new LibraryDatabaseManager();
			while (true) {
				position++;
				if (request.getParameter("deleteRow[" + position + "]") != null
						&& request.getParameter("deleteRow[" + position + "]").equals("X"))
					continue;
				try {
					String libraryName = request.getParameter("libraryName[" + position + "]");
					if (libraryName == null)
						break;
					String externalId = request.getParameter("externalId[" + position + "]");
					libraryDatabaseManager.addNewLibrary(libraryName, externalId);
					addSuccessMessage("Library " + libraryName + " added!");
				} catch (SQLIntegrityConstraintViolationException e) {
					LoadFromFile.addExceptionMessage(e.getMessage());
					DatabaseManager.addLogMessage(e);
				}
			}
			DatabaseManager.closeConnection();
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}

	}

	private void setBooksLibraries(HttpServletRequest request) {
		int row = 0;
		try {
			LibraryDatabaseManager libraryDatabaseManager = new LibraryDatabaseManager();
			while (true) {
				row++;
				if (request.getParameter("libraryId[" + row + "]") == null)
					break;

				if (request.getParameter("deleteRow[" + row + "]").equals("X"))
					continue;

				Integer libraryId = Integer.parseInt(request.getParameter("libraryId[" + row + "]"));
				Integer bookId = Integer.parseInt(request.getParameter("bookId[" + row + "]"));

				String externalId = request.getParameter("externalId[" + row + "]");

				try {
					libraryDatabaseManager.addBookToLibrary(bookId, libraryId, externalId);
					LoadFromFile.addSuccessMessage("Entry " + externalId + " added!");
				} catch (Exception e) {
					LoadFromFile.addExceptionMessage(e.getMessage());
				}
			}
			DatabaseManager.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
			DatabaseManager.addLogMessage(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LoadFromFile.errors = new ArrayList<String>();
		LoadFromFile.successes = new ArrayList<String>();

		request.setCharacterEncoding("UTF-8");
		InputProcessor inputProcessor = new InputProcessor();
		if (request.getParameter("filePathLibraries") != null
				&& !request.getParameter("filePathLibraries").equals("")) {
			String path = request.getParameter("filePathLibraries");
			try {
				inputProcessor.setFileProcessor(path);
				List<Library> libraries = inputProcessor.processLibrariesFile();
				request.setAttribute("libraries", libraries);
			} catch (WrongFileTypeException e) {
				request.setAttribute("wrongFileType", e.getFileExtension());
				DatabaseManager.addLogMessage(e);
			}
		}

		if (request.getParameter("addAllLibraries") != null)
			this.addAllLibraries(request);

		if (request.getParameter("filePathBooks") != null && !request.getParameter("filePathBooks").equals("")) {
			String path = request.getParameter("filePathBooks");
			try {
				inputProcessor.setFileProcessor(path);
				List<Book> books = inputProcessor.processBooksFile();
				request.setAttribute("books", books);
			} catch (WrongFileTypeException e) {
				request.setAttribute("wrongFileType", e.getFileExtension());
			}
		}

		if (request.getParameter("addAllBooks") != null)
			this.addAllBooks(request);

		if (request.getParameter("filePathLibrariesBooks") != null
				&& !request.getParameter("filePathLibrariesBooks").equals("")) {
			String path = request.getParameter("filePathLibrariesBooks");
			try {
				inputProcessor.setFileProcessor(path);
				List<BookLibrary> librariesBooks = inputProcessor.processLibrariesBooksFile();
				request.setAttribute("librariesBooks", librariesBooks);
				DatabaseManager.closeConnection();
			} catch (WrongFileTypeException e) {
				request.setAttribute("wrongFileType", e.getFileExtension());
				DatabaseManager.addLogMessage(e);
			} catch (Exception e) {
				DatabaseManager.addLogMessage(e);
				e.printStackTrace();
			}
		}

		if (request.getParameter("addBooksInLibraries") != null)
			this.setBooksLibraries(request);

		if (LoadFromFile.errors.size() > 0)
			request.setAttribute("exceptions", LoadFromFile.errors);
		if (LoadFromFile.successes.size() > 0)
			request.setAttribute("successes", LoadFromFile.successes);

		RequestDispatcher view = request.getRequestDispatcher("LoadFromFile.jsp");
		view.forward(request, response);
	}

}
