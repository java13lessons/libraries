package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.CustomersDatabaseManager;
import database.DatabaseManager;
import database.LibraryDatabaseManager;

/**
 * Servlet implementation class Customer
 */
@WebServlet("/Customer")
public class Customer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		int customerId = this.getCustomerIdFromQuery(request.getQueryString());
		try {

			CustomersDatabaseManager customerDbManager = new CustomersDatabaseManager();
			LibraryDatabaseManager libraryDbManager = new LibraryDatabaseManager();

			if (request.getParameter("returnBookId") != null && !request.getParameter("returnBookId").equals(""))
				libraryDbManager.returnBook(Integer.parseInt(request.getParameter("returnBookId")));
			else if (request.getParameter("bookIdBorrow") != null && !request.getParameter("bookIdBorrow").equals(""))
				libraryDbManager.borrowBook(request);

			libraryDbManager.setAllEntries(request);
			customerDbManager.setBooksBorrowed(customerId, request);
			customerDbManager.setBooksToBorrow(request);

			DatabaseManager.closeConnection();
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}

		RequestDispatcher view = request.getRequestDispatcher("Customer.jsp");

		request.setAttribute("customerId", customerId);
		view.forward(request, response);
	}

	private int getCustomerIdFromQuery(String query) {

		String[] queries = query.split("&");
		for (int i = 0; i < queries.length; i++) {

			String[] queryParamater = queries[i].split("=");
			if (queryParamater[0].equals("customerId"))
				return Integer.parseInt(queryParamater[1]);
		}

		return 0;
	}

}
