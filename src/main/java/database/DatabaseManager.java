package database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public abstract class DatabaseManager implements DatabaseActions {
	protected static Connection con = null;

	protected Date getDateSql(String dateInsert) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yy-MM-dd");

		Date dateSql = null;
		try {
			dateSql = new Date(dateFormatter.parse(dateInsert).getTime());
		} catch (Exception e) {
			DatabaseManager.addLogMessage(e);
		}
		return dateSql;
	}

	public static void init() throws Exception {
		if (DatabaseManager.con != null && !DatabaseManager.con.isClosed())
			return;

		Class.forName("com.mysql.cj.jdbc.Driver");
		DatabaseManager.con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/libraries", "root", "");
		DatabaseManager.con.setAutoCommit(false);
	}

	public DatabaseManager() throws Exception {
		init();
	}

	public static void closeConnection() throws SQLException {
		DatabaseManager.con.commit();
		DatabaseManager.con.close();
	}

	public static PreparedStatement getPreparedStatement(String sqlQuery) throws Exception {
		return DatabaseManager.con.prepareCall(sqlQuery);
	}

	public static void addLogMessage(Exception e) {
		StringWriter strWriter = new StringWriter();
		PrintWriter printWritter = new PrintWriter(strWriter);
		e.printStackTrace(printWritter);
		addLogMessage(e.getMessage(), strWriter.toString());
	}

	public static void addLogMessage(String message, String stackTrace) {

		try {
			PreparedStatement stmt = DatabaseManager.con
					.prepareStatement("INSERT INTO log (Message,Stacktrace,Timestamp) VALUES (?,?,?)");
			stmt.setString(1, message);
			stmt.setString(2, stackTrace);
			Timestamp timeStmp = new Timestamp(System.currentTimeMillis());
			stmt.setTimestamp(3, timeStmp);
			stmt.execute();

		} catch (Exception e) {
		}

	}

}
