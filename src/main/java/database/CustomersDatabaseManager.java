package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objectstructures.BookLibrary;
import objectstructures.BorrowEntry;
import objectstructures.Customer;

public class CustomersDatabaseManager extends DatabaseManager {

	public CustomersDatabaseManager() throws Exception {
		super();
	}

	@Override
	public void deleteEntry(int customerId) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM customers WHERE ID = ?");
		stmt.setInt(1, customerId);
		stmt.execute();
	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int customerIdRename = Integer.parseInt(request.getParameter("customerRenamed"));
		String newCustomerName = request.getParameter("renameName[" + customerIdRename + "]");
		String newCustomerSurname = request.getParameter("renameSurname[" + customerIdRename + "]");

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("UPDATE customers SET Name = ?,Surname =? WHERE ID = ?");
		stmt.setString(1, newCustomerName);
		stmt.setString(2, newCustomerSurname);
		stmt.setInt(3, customerIdRename);
		stmt.executeUpdate();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO customers (Name,Surname) VALUES (?,?)");
		stmt.setString(1, request.getParameter("customerNameInsert"));
		stmt.setString(2, request.getParameter("customerSurnameInsert"));
		stmt.execute();
		request.setAttribute("customerInserted", true);

	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {

		Map<Integer, Customer> allCustomers = new TreeMap<Integer, Customer>();
		ResultSet allCustomersResult = DatabaseManager.con.createStatement().executeQuery("SELECT * FROM customers");

		while (allCustomersResult.next())
			allCustomers.put(allCustomersResult.getInt("ID"),
					new Customer(allCustomersResult.getString("name"), allCustomersResult.getString("surname")));

		request.setAttribute("customersList", allCustomers);

	}

	public void setBooksBorrowed(int customerId, HttpServletRequest request) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement(
				"SELECT customers_books.Book_lib_ID, books.Name, customers.Name, customers.Surname, customers_books.Return_date FROM books "
						+ "INNER JOIN books_libraries ON books_libraries.book_id = books.ID INNER JOIN"
						+ " customers_books ON customers_books.Book_lib_ID = books_libraries.ID INNER JOIN customers ON customers.ID = "
						+ "customers_books.Customer_ID WHERE customers_books.Customer_ID = ?");
		stmt.setInt(1, customerId);
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, BorrowEntry> booksBorrowed = new TreeMap<Integer, BorrowEntry>();
		while (resultSet.next())
			booksBorrowed.put(resultSet.getInt("customers_books.Book_lib_ID"),
					new BorrowEntry(resultSet.getString("books.Name"), resultSet.getString("customers.Name"),
							resultSet.getString("customers.Surname"),
							resultSet.getString("customers_books.Return_date")));

		request.setAttribute("booksBorrowed", booksBorrowed);
	}

	public void setBooksToBorrow(HttpServletRequest request) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement(
				"SELECT books_libraries.ID,books_libraries.library_id,books_libraries.book_id, books.name,libraries.name FROM books_libraries "
						+ "LEFT JOIN customers_books ON customers_books.Book_lib_ID = books_libraries.ID INNER JOIN books ON books.ID = books_libraries.book_id"
						+ " INNER JOIN libraries ON libraries.id = books_libraries.id WHERE customers_books.Book_lib_ID IS NULL");
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, BookLibrary> booksNotBorrowed = new TreeMap<Integer, BookLibrary>();
		while (resultSet.next())
			booksNotBorrowed.put(resultSet.getInt("books_libraries.ID"),
					new BookLibrary(resultSet.getInt("books_libraries.library_id"),
							resultSet.getInt("books_libraries.book_id"), resultSet.getString("libraries.name"),
							resultSet.getString("books.name")));
		request.setAttribute("booksNotBorrowed", booksNotBorrowed);
	}
}
