package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import objectstructures.Log;

public class ErrorAdminDatabaseManager {

	public ErrorAdminDatabaseManager() throws Exception {
		DatabaseManager.init();
	}

	public List<Log> getLogEntries() throws Exception {
		List<Log> log = new ArrayList<Log>();

		PreparedStatement stmt = DatabaseManager.getPreparedStatement("SELECT * FROM log");
		ResultSet resultSet = stmt.executeQuery();
		while (resultSet.next()) {
			Log logEntry = new Log();
			logEntry.setMessage(resultSet.getString("Message"));
			logEntry.setStackTrace(resultSet.getString("Stacktrace"));
			logEntry.setTimeStamp(resultSet.getString("Timestamp"));
			log.add(logEntry);
		}

		return log;
	}

}
