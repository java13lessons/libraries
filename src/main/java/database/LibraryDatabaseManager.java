package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objectstructures.BookLibrary;
import objectstructures.BorrowEntry;
import objectstructures.Library;
import pages.LoadFromFile;

public class LibraryDatabaseManager extends DatabaseManager {

	public LibraryDatabaseManager() throws Exception {
		super();
	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int libraryIdRename = Integer.parseInt(request.getParameter("libraryRenamed"));
		String newLibraryName = request.getParameter("rename[" + libraryIdRename + "]");

		PreparedStatement stmt = DatabaseManager.con.prepareStatement("UPDATE libraries SET Name = ? WHERE ID = ?");
		stmt.setString(1, newLibraryName);
		stmt.setInt(2, libraryIdRename);
		stmt.executeUpdate();
	}

	@Override
	public void deleteEntry(int libraryId) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM libraries WHERE ID = ?");
		stmt.setInt(1, libraryId);
		stmt.execute();
	}

	public void addNewLibrary(String name, String externalId) throws SQLException {
		if (externalId == null) {
			this.addNewLibrary(name);
			return;
		}

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO libraries (Name,External_ID) VALUES (?,?)");
		stmt.setString(1, name);
		stmt.setString(2, externalId);
		stmt.execute();
	}

	public void addNewLibrary(String name) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("INSERT INTO libraries (Name) VALUES (?)");
		stmt.setString(1, name);
		stmt.execute();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.addNewLibrary(request.getParameter("libraryName"));
		request.setAttribute("libraryInserted", true);

	}

	public void setBooksLibrariesIds(List<BookLibrary> booksLibraries) throws Exception {

		List<BookLibrary> bookLibrariesInternal = new ArrayList<BookLibrary>(booksLibraries);
		Map<String, Integer> booksExternalIds = new TreeMap<String, Integer>();
		Map<String, Integer> librariesExternalIds = new TreeMap<String, Integer>();
		Iterator<BookLibrary> iterator = booksLibraries.iterator();
		while (iterator.hasNext()) {
			BookLibrary bookLibrary = iterator.next();
			booksExternalIds.put(bookLibrary.getBookExternalId(), null);
			librariesExternalIds.put(bookLibrary.getLibraryExternalId(), null);
		}

		StringBuffer booksQueryBuilder = new StringBuffer();
		boolean firstLine = true;
		for (int i = 0; i < booksExternalIds.size(); i++) {
			if (firstLine) {
				booksQueryBuilder.append("?");
				firstLine = false;
			} else
				booksQueryBuilder.append(",?");
		}

		PreparedStatement stmtBooks = DatabaseManager.con
				.prepareStatement("SELECT * FROM books WHERE External_ID IN (" + booksQueryBuilder.toString() + ") ");

		int paramaterCount = 0;
		Iterator<String> iteratorBooks = booksExternalIds.keySet().iterator();
		while (iteratorBooks.hasNext()) {
			paramaterCount++;
			stmtBooks.setString(paramaterCount, iteratorBooks.next());
		}

		ResultSet resultSet = stmtBooks.executeQuery();
		Map<Integer, String> booksNames = new TreeMap<Integer, String>();
		while (resultSet.next()) {
			booksNames.put(resultSet.getInt("ID"), resultSet.getString("Name"));
			booksExternalIds.put(resultSet.getString("External_ID"), resultSet.getInt("ID"));
		}

		StringBuffer librariesQueryBuilder = new StringBuffer();
		firstLine = true;
		for (int i = 0; i < librariesExternalIds.size(); i++) {
			if (firstLine) {
				librariesQueryBuilder.append("?");
				firstLine = false;
			} else
				librariesQueryBuilder.append(",?");
		}

		PreparedStatement stmtLibraries = DatabaseManager.con.prepareStatement(
				"SELECT * FROM libraries WHERE External_ID IN (" + librariesQueryBuilder.toString() + ") ");

		paramaterCount = 0;
		Iterator<String> iteratorLibraries = librariesExternalIds.keySet().iterator();
		while (iteratorLibraries.hasNext()) {
			paramaterCount++;
			stmtLibraries.setString(paramaterCount, iteratorLibraries.next());
		}

		resultSet = stmtLibraries.executeQuery();
		Map<Integer, String> librariesNames = new TreeMap<Integer, String>();
		while (resultSet.next()) {
			librariesNames.put(resultSet.getInt("ID"), resultSet.getString("Name"));
			librariesExternalIds.put(resultSet.getString("External_ID"), resultSet.getInt("ID"));
		}

		iterator = bookLibrariesInternal.iterator();
		while (iterator.hasNext()) {
			BookLibrary bookLibrary = iterator.next();

			boolean skipCurrent = false;
			if (booksExternalIds.get(bookLibrary.getBookExternalId()) == null) {
				LoadFromFile.addExceptionMessage(
						"Book with External ID " + bookLibrary.getBookExternalId() + " does not exist");
				skipCurrent = true;
			}
			if (librariesExternalIds.get(bookLibrary.getLibraryExternalId()) == null) {
				LoadFromFile.addExceptionMessage(
						"Library with External ID " + bookLibrary.getLibraryExternalId() + " does not exist");
				skipCurrent = true;
			}

			if (skipCurrent) {
				booksLibraries.remove(bookLibrary);
				continue;
			}

			int bookId = booksExternalIds.get(bookLibrary.getBookExternalId());

			String bookName = booksNames.get(bookId);
			int libraryId = librariesExternalIds.get(bookLibrary.getLibraryExternalId());
			String libraryName = librariesNames.get(libraryId);

			bookLibrary.setBookId(bookId);
			bookLibrary.setBookName(bookName);

			bookLibrary.setLibraryId(libraryId);
			bookLibrary.setLibraryName(libraryName);
		}

	}

	public void setAllBooksForLibrary(int libraryId, HttpServletRequest request) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement(
				"SELECT * FROM books_libraries INNER JOIN books ON books.ID = books_libraries.book_id WHERE library_id = ?");
		stmt.setInt(1, libraryId);
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, String> books = new TreeMap<Integer, String>();
		while (resultSet.next())
			books.put(resultSet.getInt("books_libraries.ID"), resultSet.getString("books.Name"));

		request.setAttribute("booksInLibrary", books);
	}

	public Map<Integer, Library> getLibraries() throws SQLException {
		Map<Integer, Library> allLibraries = new TreeMap<Integer, Library>();
		ResultSet allLibrariesResult = DatabaseManager.con.createStatement().executeQuery("SELECT * FROM libraries");

		while (allLibrariesResult.next())
			allLibraries.put(allLibrariesResult.getInt("ID"), new Library(allLibrariesResult.getInt("ID"),
					allLibrariesResult.getString("name"), allLibrariesResult.getString("External_ID")));

		return allLibraries;
	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {
		request.setAttribute("libraryList", this.getLibraries());
	}

	public void addBookToLibrary(int bookId, int libraryId, String externalId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO books_libraries (library_id,book_id,External_ID) VALUES (?,?,?)");
		stmt.setInt(1, libraryId);
		stmt.setInt(2, bookId);
		stmt.setString(3, externalId);
		stmt.execute();
	}

	public void addBookToLibrary(int bookId, int libraryId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO books_libraries (library_id,book_id) VALUES (?,?)");
		stmt.setInt(1, libraryId);
		stmt.setInt(2, bookId);
		stmt.execute();
	}

	public void deleteBookLibraryEntry(int entryId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM books_libraries WHERE ID = ?");
		stmt.setInt(1, entryId);
		stmt.execute();
	}

	public void setBooksNotBorrowed(int libraryId, HttpServletRequest request) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("SELECT books_libraries.ID,books.name FROM books_libraries "
						+ "LEFT JOIN customers_books ON customers_books.Book_lib_ID = books_libraries.ID INNER JOIN books ON books.ID = books_libraries.book_id"
						+ " WHERE books_libraries.library_id = ? AND customers_books.Book_lib_ID IS NULL");
		stmt.setInt(1, libraryId);
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, String> booksNotBorrowed = new TreeMap<Integer, String>();
		while (resultSet.next())
			booksNotBorrowed.put(resultSet.getInt("books_libraries.ID"), resultSet.getString("books.name"));
		request.setAttribute("booksNotBorrowed", booksNotBorrowed);

	}

	public void setBooksBorrowed(int libraryId, HttpServletRequest request) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement(
				"SELECT customers_books.Book_lib_ID, books.Name, customers.Name, customers.Surname, customers_books.Return_date FROM books "
						+ "INNER JOIN books_libraries ON books_libraries.book_id = books.ID INNER JOIN"
						+ " customers_books ON customers_books.Book_lib_ID = books_libraries.ID INNER JOIN customers ON customers.ID = "
						+ "customers_books.Customer_ID WHERE books_libraries.library_id = ?");
		stmt.setInt(1, libraryId);
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, BorrowEntry> booksBorrowed = new TreeMap<Integer, BorrowEntry>();
		while (resultSet.next())
			booksBorrowed.put(resultSet.getInt("customers_books.Book_lib_ID"),
					new BorrowEntry(resultSet.getString("books.Name"), resultSet.getString("customers.Name"),
							resultSet.getString("customers.Surname"),
							resultSet.getString("customers_books.Return_date")));

		request.setAttribute("booksBorrowed", booksBorrowed);
	}

	public void borrowBook(HttpServletRequest request) throws Exception {
		int bookIdToBorrow = Integer.parseInt(request.getParameter("bookIdBorrow"));
		int customerIdBorrow = Integer.parseInt(request.getParameter("customerIdBorrow"));
		String returnDate = request.getParameter("dateReturnBorrow");

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO customers_books (Book_lib_ID,Customer_ID,Return_date) VALUES (?,?,?)");

		stmt.setInt(1, bookIdToBorrow);
		stmt.setInt(2, customerIdBorrow);
		stmt.setDate(3, this.getDateSql(returnDate));
		stmt.execute();

	}

	public void returnBook(int bookId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("DELETE FROM customers_books WHERE Book_lib_ID = ? ");
		stmt.setInt(1, bookId);
		stmt.execute();
	}

}
