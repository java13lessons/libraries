package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objectstructures.Book;

public class BooksDatabaseManager extends DatabaseManager {

	public BooksDatabaseManager() throws Exception {
		super();
	}

	@Override
	public void deleteEntry(int id) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM books WHERE ID = ?");
		stmt.setInt(1, id);
		stmt.execute();
	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int bookIdRename = Integer.parseInt(request.getParameter("bookChanged"));
		String newName = request.getParameter("renameName[" + bookIdRename + "]");
		String newAuthor = request.getParameter("renameAuthor[" + bookIdRename + "]");
		String dateInput = request.getParameter("changeDate[" + bookIdRename + "]");

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("UPDATE books SET Name = ?,Author = ?,Date_published = ? WHERE ID = ?");
		stmt.setString(1, newName);
		stmt.setString(2, newAuthor);
		stmt.setDate(3, this.getDateSql(dateInput));
		stmt.setInt(4, bookIdRename);
		stmt.executeUpdate();
	}

	public void addNewEntry(String name, String author, String datePublished, String externalId) throws Exception {
		if (externalId == null) {
			this.addNewEntry(name, author, datePublished);
			return;
		}

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO books (Name,Author,Date_published,External_ID) VALUES (?,?,?,?)");
		stmt.setString(1, name);
		stmt.setString(2, author);
		stmt.setDate(3, this.getDateSql(datePublished));
		stmt.setString(4, externalId);
		stmt.execute();
	}

	public void addNewEntry(String name, String author, String datePublished) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO books (Name,Author,Date_published) VALUES (?,?,?)");
		stmt.setString(1, name);
		stmt.setString(2, author);
		stmt.setDate(3, this.getDateSql(datePublished));
		stmt.execute();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {

		this.addNewEntry(request.getParameter("bookNameInsert"), request.getParameter("bookAuthorInsert"),
				request.getParameter("bookDateInsert"));
		request.setAttribute("bookInserted", true);

	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {

		Map<Integer, Book> allBooks = new TreeMap<Integer, Book>();
		ResultSet allBooksResult = DatabaseManager.con.createStatement().executeQuery("SELECT * FROM books");

		while (allBooksResult.next())
			allBooks.put(allBooksResult.getInt("ID"), new Book(allBooksResult.getString("Name"),
					allBooksResult.getString("Author"), allBooksResult.getDate("Date_published").toString()));

		request.setAttribute("booksList", allBooks);

	}

}
