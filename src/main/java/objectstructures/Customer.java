package objectstructures;

public class Customer {

	private String name, surname;

	public Customer(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

}
