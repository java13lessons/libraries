package objectstructures;

public class BookLibrary {

	private int bookId, LibraryId;
	private String bookName, libraryName, externalId, bookExternalId, libraryExternalId;

	@Override
	public String toString() {
		return "LibraryExternalId: " + this.libraryExternalId + System.lineSeparator() + "BookExtId:"
				+ this.bookExternalId;
	}

	public BookLibrary() {
		// TODO Auto-generated constructor stub
	}

	public void setBookExternalId(String bookExternalId) {
		this.bookExternalId = bookExternalId;
	}

	public void setLibraryExternalId(String libraryExternalId) {
		this.libraryExternalId = libraryExternalId;
	}

	public String getBookExternalId() {
		return bookExternalId;
	}

	public String getLibraryExternalId() {
		return libraryExternalId;
	}

	public BookLibrary(int libraryId, int bookId, String libraryName, String bookName) {
		this.bookId = bookId;
		this.LibraryId = libraryId;
		this.libraryName = libraryName;
		this.bookName = bookName;
	}

	public BookLibrary(int libraryId, int bookId, String libraryName, String bookName, String externalId) {
		this.bookId = bookId;
		this.LibraryId = libraryId;
		this.libraryName = libraryName;
		this.bookName = bookName;
		this.externalId = externalId;
	}

	public int getBookId() {
		return bookId;
	}

	public int getLibraryId() {
		return LibraryId;
	}

	public String getLibraryName() {
		return libraryName;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setLibraryId(int libraryId) {
		LibraryId = libraryId;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getExternalId() {
		return externalId;
	}

}
