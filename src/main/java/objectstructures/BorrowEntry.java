package objectstructures;

public class BorrowEntry {

	private String bookName, customerName, customerSurname, dateToReturn;

	public BorrowEntry(String bookName, String customerName, String customerSurname, String dateToReturn) {
		this.bookName = bookName;
		this.customerName = customerName;
		this.customerSurname = customerSurname;
		this.dateToReturn = dateToReturn;
	}

	public String getBookName() {
		return bookName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getCustomerSurname() {
		return customerSurname;
	}

	public String getDateToReturn() {
		return dateToReturn;
	}

}
