package objectstructures;

public class Book {

	private String name, author, datePublished, externalId;

	public Book() {
	}

	public Book(String name, String author, String datePublished) {
		this.author = author;
		this.datePublished = datePublished;
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public String getDatePublished() {
		return datePublished;
	}

	public String getName() {
		return name;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

}
