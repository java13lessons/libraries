package objectstructures;

public class Library {

	private int id;
	private String externalId, name;

	public Library() {
	}

	public Library(String name, String externalId) {
		this.name = name;
		this.externalId = externalId;
	}

	public Library(int id, String name, String externalId) {
		this.id = id;
		this.name = name;
		this.externalId = externalId;
	}

	public String getExternalId() {
		return externalId;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

}
