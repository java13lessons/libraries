<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="objectstructures.Customer"%>
<%@page import="objectstructures.BorrowEntry"%>
<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Book"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>

<%
Map<Integer, Book> allBooks = (Map<Integer, Book>) request.getAttribute("booksList");
if (allBooks == null)
	allBooks = new TreeMap<Integer, Book>();

Map<Integer, String> booksInLibrary = (Map<Integer, String>) request.getAttribute("booksInLibrary");

Map<Integer, BorrowEntry> booksBorrowed = (Map<Integer, BorrowEntry>) request.getAttribute("booksBorrowed");
Map<Integer, String> booksNotBorrwed = (Map<Integer, String>) request.getAttribute("booksNotBorrowed");
Map<Integer, Customer> allCustomers = (Map<Integer, Customer>) request.getAttribute("customersList");
%>

<jsp:include page="header-template.jsp" />
<body>
	<div class="container">
		<form id="libraryForm"
			action="/LibrariesWebApp/Library?<%=request.getQueryString()%>"
			method="POST">

			<input hidden id="entryToDelete" name="entryToDelete">
			<h2>Books in Library:</h2>
			<ul class="list-group"
				style="width: 30%; word-wrap: break-word; text-align: center;">
				<%
				Iterator<Entry<Integer, String>> iteratorLibrary = booksInLibrary.entrySet().iterator();
				while (iteratorLibrary.hasNext()) {
					Entry<Integer, String> bookInLibrary = iteratorLibrary.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayBookInName[<%=bookInLibrary.getKey()%>]">
							<%=bookInLibrary.getValue()%>
						</div>
						<button type="button" class="fa fa-trash icon-button"
							onclick="document.getElementById('entryToDelete').value = <%=bookInLibrary.getKey()%>;
						document.getElementById('libraryForm').submit()"></button>
					</div>
				</li>
				<%
				}
				%>
			</ul>

			<h2>Books Borrowed:</h2>
			<input hidden name="returnBookId" id="returnBookId">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">Book name</th>
						<th scope="col">Customer name</th>
						<th scope="col">Customer surname</th>
						<th scope="col">Return date</th>
						<th />
					</tr>
				</thead>
				<tbody>
					<%
					Iterator<Entry<Integer, BorrowEntry>> iteratorBooksBorrowed = booksBorrowed.entrySet().iterator();
					while (iteratorBooksBorrowed.hasNext()) {
						Entry<Integer, BorrowEntry> bookBorrowed = iteratorBooksBorrowed.next();
					%>

					<tr>
						<td><%=bookBorrowed.getValue().getBookName()%></td>
						<td><%=bookBorrowed.getValue().getCustomerName()%></td>
						<td><%=bookBorrowed.getValue().getCustomerSurname()%></td>
						<td><%=bookBorrowed.getValue().getDateToReturn()%></td>
						<td><button type="button"
								onclick="document.getElementById('returnBookId').value = <%=bookBorrowed.getKey()%>;
						document.getElementById('libraryForm').submit()">Return</button></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>

			<h2>Books available:</h2>
			<input id="borrowBookButton" name="borrowBookButton" type="checkbox"
				hidden> <input id="bookIdBorrow" name="bookIdBorrow" hidden>
			<input id="customerIdBorrow" name="customerIdBorrow" hidden>
			<input id="dateReturnBorrow" name="dateReturnBorrow" hidden>

			<ul class="list-group"
				style="width: 80%; word-wrap: break-word; text-align: center;">
				<%
				Iterator<Entry<Integer, String>> iteratorBooksNotBorrowed = booksNotBorrwed.entrySet().iterator();
				while (iteratorBooksNotBorrowed.hasNext()) {
					Entry<Integer, String> bookAvailable = iteratorBooksNotBorrowed.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div class="col-sm-4">
							<%=bookAvailable.getValue()%>
						</div>
						<div class="col-sm-4">
							<select id="bookBorrowCustomer[<%=bookAvailable.getKey()%>]"
								name="bookBorrowCustomer[<%=bookAvailable.getKey()%>]">
								<option />
								<%
								Iterator<Entry<Integer, Customer>> iteratorCustomers = allCustomers.entrySet().iterator();
								while (iteratorCustomers.hasNext()) {
									Entry<Integer, Customer> customer = iteratorCustomers.next();
								%>
								<option value="<%=customer.getKey()%>"><%=customer.getValue().getName() + " " + customer.getValue().getSurname()%></option>
								<%
								}
								%>

							</select>
						</div>
						<div class="col-sm-3">
							<input id="returnDate[<%=bookAvailable.getKey()%>]" type="date">
						</div>
						<div class="col-sm-1">
							<button type="button"
								onclick="onBorrow(<%=bookAvailable.getKey()%>)">Borrow</button>
						</div>
					</div>
				</li>
				<%
				}
				%>
			</ul>

			<input hidden id="bookToAdd" name="bookToAdd">
			<h2>All books available:</h2>
			<ul class="list-group"
				style="width: 30%; word-wrap: break-word; text-align: center;">
				<%
				Iterator<Entry<Integer, Book>> iterator = allBooks.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Book> book = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayBookName[<%=book.getKey()%>]">
							<%=book.getValue().getName()%>
						</div>
						<button type="button" class="fa fa-plus-circle icon-button"
							onclick="document.getElementById('bookToAdd').value = <%=book.getKey()%>;
						document.getElementById('libraryForm').submit()"></button>
					</div>
				</li>
				<%
				}
				%>
			</ul>
		</form>
	</div>
</body>
<script>
function onBorrow(bookId){
	var bookBorrowCustomerSelection = document.getElementById("bookBorrowCustomer[" + bookId + "]");
	var customerIdSelected = bookBorrowCustomerSelection.value;
	var dateReturn = document.getElementById("returnDate[" + bookId + "]").value;
	document.getElementById('bookIdBorrow').value = bookId;
	document.getElementById('customerIdBorrow').value = customerIdSelected;
	document.getElementById('dateReturnBorrow').value = dateReturn;
	document.getElementById('borrowBookButton').checked = true;
	document.getElementById('libraryForm').submit();
	}
</script>