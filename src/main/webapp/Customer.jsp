<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<%@page import="objectstructures.BorrowEntry"%>
<%@page import="objectstructures.BookLibrary"%>
<%@page import="java.util.Map"%>
<jsp:include page="header-template.jsp" />
<%
Map<Integer, BorrowEntry> booksBorrowed = (Map<Integer, BorrowEntry>) request.getAttribute("booksBorrowed");
Map<Integer, BookLibrary> booksNotBorrowed = (Map<Integer, BookLibrary>) request.getAttribute("booksNotBorrowed");
%>
<body>
	<div class="container">
		<form id="customerForm" method="POST"
			action="/LibrariesWebApp/Customer?<%=request.getQueryString()%>">

			<h2>Books borrowed:</h2>
			<input hidden name="returnBookId" id="returnBookId">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">Book name</th>
						<th scope="col">Return date</th>
						<th />
					</tr>
				</thead>
				<tbody>
					<%
					Iterator<Entry<Integer, BorrowEntry>> iteratorBooksBorrowed = booksBorrowed.entrySet().iterator();
					while (iteratorBooksBorrowed.hasNext()) {
						Entry<Integer, BorrowEntry> bookBorrowed = iteratorBooksBorrowed.next();
					%>

					<tr>
						<td><%=bookBorrowed.getValue().getBookName()%></td>
						<td><%=bookBorrowed.getValue().getDateToReturn()%></td>
						<td><button type="button"
								onclick="document.getElementById('returnBookId').value = <%=bookBorrowed.getKey()%>;
						document.getElementById('customerForm').submit()">Return</button></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
			<h2>Borrow book:</h2>
			<input hidden id="bookIdBorrow" name="bookIdBorrow"> <input
				hidden id="customerIdBorrow" name="customerIdBorrow" value="<%=(Integer) request.getAttribute("customerId")%>"> <input
				hidden id="dateReturnBorrow" name="dateReturnBorrow">
			<table class="table">
				<thead>
					<tr>
						<th>Library</th>
						<th>Book</th>
						<th>Date return</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<%
					Iterator<Entry<Integer, BookLibrary>> iteratorBooksNotBorrowed = booksNotBorrowed.entrySet().iterator();
					while (iteratorBooksNotBorrowed.hasNext()) {
						Entry<Integer, BookLibrary> bookAvailable = iteratorBooksNotBorrowed.next();
					%>
					<tr>
						<td><%=bookAvailable.getValue().getLibraryName()%></td>
						<td><%=bookAvailable.getValue().getBookName()%></td>
						<td><input type="date"
							id="borrowReturnDate[<%=bookAvailable.getKey()%>]"></td>
						<td><button type="button"
								onclick="document.getElementById('bookIdBorrow').value = <%=bookAvailable.getKey()%>;
								document.getElementById('dateReturnBorrow').value = document.getElementById('borrowReturnDate[<%=bookAvailable.getKey()%>]').value
								document.getElementById('customerForm').submit()">Borrow</button></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
	</div>
	</form>
	</div>
</body>