<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Book"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="header-template.jsp" />

<%
Map<Integer, Book> allBooks = (Map<Integer, Book>) request.getAttribute("booksList");
if (allBooks == null)
	allBooks = new TreeMap<Integer, Book>();
%>

<body>
	<div class="container">
		<form id="bookForm" action="/LibrariesWebApp/Books" method="POST">
			<input hidden id="deleteBookId" name="deleteBookId"> <input
				hidden id="bookChanged" name="bookChanged">
			<ul class="list-group" style="width: 70%; word-wrap: break-word">
				<%
				Iterator<Entry<Integer, Book>> iterator = allBooks.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Book> book = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayBookName[<%=book.getKey()%>]">
							<%=book.getValue().getName()%>
						</div>
						<div style="display: contents"
							id="displayBookAuthor[<%=book.getKey()%>]">
							<%=book.getValue().getAuthor()%>
						</div>
						<div style="display: contents"
							id="displayBookDate[<%=book.getKey()%>]">
							<%=book.getValue().getDatePublished()%>
						</div>
						<input hidden id="renameName[<%=book.getKey()%>]"
							name="renameName[<%=book.getKey()%>]"
							value="<%=book.getValue().getName()%>"> <input hidden
							id="renameAuthor[<%=book.getKey()%>]"
							name="renameAuthor[<%=book.getKey()%>]"
							value="<%=book.getValue().getAuthor()%>"> <input hidden
							id="changeDate[<%=book.getKey()%>]"
							name="changeDate[<%=book.getKey()%>]" type="date"
							value="<%=book.getValue().getDatePublished()%>">
						<button type="button"
							onclick="document.getElementById('deleteBookId').value = <%=book.getKey()%>;
						document.getElementById('bookForm').submit()"
							class="fa fa-trash icon-button"></button>
						<button type="button" id="changeButton[<%=book.getKey()%>]"
							onclick="onChange(<%=book.getKey()%>)" class="btn"
							style="text-align: right;">Change</button>
						<button type="button" class="btn"
							id="saveBook[<%=book.getKey()%>]" style="display: none"
							onclick="document.getElementById('bookChanged').value = <%=book.getKey()%>;
						document.getElementById('bookForm').submit()">Save</button>
						<button type="button" class="btn"
							onclick="onCancel(<%=book.getKey()%>)"
							id="cancelInput[<%=book.getKey()%>]" style="display: none">Cancel</button>
					</div>
				</li>
				<%
				}
				%>
			</ul>

			<div class="row">
				<label for="insertNewBook"> Book name/author/date published
				</label> <input id="bookNameInsert" name="bookNameInsert"> <input
					id="bookAuthorInsert" name="bookAuthorInsert"> <input
					id="bookDateInsert" name="bookDateInsert" type="date">
				<button name="insertNewBook" type="submit" class="btn btn-success">Insert</button>
			</div>
		</form>
	</div>
	<script>
	function onChange(key){
		document.getElementById("renameName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("renameAuthor[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("changeDate[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayBookName[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayBookAuthor[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayBookDate[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveBook[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("changeButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("renameName[".concat(key).concat("]")).style.display='none';
		document.getElementById("renameAuthor[".concat(key).concat("]")).style.display='none';
		document.getElementById("changeDate[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayBookName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayBookAuthor[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayBookDate[".concat(key).concat("]")).style.display='inline-block';
	    document.getElementById("saveBook[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("changeButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>