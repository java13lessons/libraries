<%@page import="objectstructures.Library"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%
Map<Integer, Library> allLibraries = (Map<Integer, Library>) request.getAttribute("libraryList");
if (allLibraries == null)
	allLibraries = new TreeMap<Integer, Library>();
%>
<jsp:include page="header-template.jsp" />
<body>
	<div class="container">
		<form id="libraryForm" action="/LibrariesWebApp/AllLibraries"
			method="POST">
			<input hidden id="deleteLibraryId" name="deleteLibraryId"> <input
				hidden id="libraryRenamed" name="libraryRenamed">
			<ul class="list-group" style="width: 70%; word-wrap: break-word">
				<%
				Iterator<Entry<Integer, Library>> iterator = allLibraries.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Library> library = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayLibraryName[<%=library.getKey()%>]">
							<a
								href="/LibrariesWebApp/Library?libraryId=<%=library.getKey()%>"><%=library.getValue().getName()%>
							</a>
							<%
							if (library.getValue().getExternalId() != null) {
							%>
							(External ID : "<%=library.getValue().getExternalId()%>")
							<%
							}
							%>
						</div>
						<input hidden id="rename[<%=library.getKey()%>]"
							name="rename[<%=library.getKey()%>]"
							value="<%=library.getValue().getName()%>">
						<button type="button"
							onclick="document.getElementById('deleteLibraryId').value = <%=library.getKey()%>;
						document.getElementById('libraryForm').submit()"
							class="fa fa-trash icon-button"></button>
						<button type="button" id="renameButton[<%=library.getKey()%>]"
							onclick="onRename(<%=library.getKey()%>)" class="btn"
							style="text-align: right;">Rename</button>
						<button type="button" class="btn"
							id="saveName[<%=library.getKey()%>]" style="display: none"
							onclick="document.getElementById('libraryRenamed').value = <%=library.getKey()%>;
						document.getElementById('libraryForm').submit()">Save</button>
						<button type="button" class="btn"
							onclick="onCancel(<%=library.getKey()%>)"
							id="cancelInput[<%=library.getKey()%>]" style="display: none">Cancel</button>
					</div>
				</li>
				<%
				}
				%>
			</ul>
			<div class="row">
				<label for="libraryName">Library name</label> <input
					id="libraryName" name="libraryName">
				<button name="insertNewLibrary" type="submit"
					class="btn btn-success">Insert</button>
			</div>
			<div class="row">
				<label for="exportData">Export to:</label> <select name="exportType">
					<option value="" selected></option>
					<option value="CSV">CSV</option>
					<option value="XML">XML</option>
					<option value="JSON">JSON</option>
				</select> <input name="fileName">
				<button name="export" class="btn btn-success">Export</button>
			</div>
		</form>
		<%
		if (request.getAttribute("libraryInserted") != null && (boolean) request.getAttribute("libraryInserted") == true) {
		%>
		<div>
			<h1 style="color: green">Entry is entered!</h1>
		</div>
		<%
		}
		%>
	</div>
	<script>
	
	function onRename(key){
		document.getElementById("rename[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayLibraryName[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveName[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("rename[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayLibraryName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("saveName[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>
</html>