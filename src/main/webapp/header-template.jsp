<html lang="en">
<head>
<link rel="stylesheet" href="styles/css/bootstrap.min.css">
<link rel="stylesheet" href="styles/css/bootstrap.css">
<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="shortcut icon" type="image/png" href="files/logos/book.png">
<link rel="stylesheet" href="styles/css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Libraries</title>
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a class="nav-link" href="/LibrariesWebApp/AllLibraries">Libraries</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li><a class="nav-link" href="/LibrariesWebApp/Customers">Customers</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li class="nav-item"><a class="nav-link"
					href="/LibrariesWebApp/Books"> Books</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li class="nav-item"><a class="nav-link"
					href="/LibrariesWebApp/LoadFromFile">Load from file</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li class="nav-item"><a class="nav-link"
					href="/LibrariesWebApp/ErrorAdmin">Error Administration</a></li>
			</ul>
		</div>
	</nav>
</body>
</html>