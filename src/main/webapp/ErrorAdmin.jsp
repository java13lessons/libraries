<%@page import="java.util.Iterator"%>
<%@page import="objectstructures.Log"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:include page="header-template.jsp" />
<%
List<Log> logEntries = (List<Log>) request.getAttribute("errors");
Iterator<Log> iterator = logEntries.iterator();
%>
<body>
	<div class="container">
		<table class="table">
			<thead>
				<th>Message</th>
				<th>Stacktrace</th>
				<th>Timestamp</th>
			</thead>
			<tbody>
				<%
				while (iterator.hasNext()) {
					Log logEntry = iterator.next();
				%>
				<tr>
					<td><%=logEntry.getMessage()%></td>
					<td><%=logEntry.getStackTrace()%></td>
					<td><%=logEntry.getTimeStamp()%></td>
				</tr>
				<%
				}
				%>
			</tbody>
		</table>
	</div>

</body>