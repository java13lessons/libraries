<%@page import="objectstructures.BookLibrary"%>
<%@page import="objectstructures.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="objectstructures.Library"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:include page="header-template.jsp" />
<body>
	<div class="container">
		<form id="inputFileFormLibraries"
			action="/LibrariesWebApp/LoadFromFile" method="POST">
			<div class="row">
				<div class="col-sm-3">
					<label for="filePathLibraries">Libraries:</label>
				</div>
				<div class="col-sm-3">
					<input type="file" id="filePathLibraries" name="filePathLibraries">
				</div>
				<div class="col-sm-6" style="text-align: left">
					<button class="btn">Load</button>
				</div>
			</div>
		</form>
		<%
		if (request.getAttribute("libraries") != null) {
			List<Library> libraries = (List<Library>) request.getAttribute("libraries");
			Iterator<Library> iteratorLibraries = libraries.iterator();
		%>
		<form id="addAllLibraries" action="/LibrariesWebApp/LoadFromFile"
			method="POST">
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>External ID</th>
						<th />
					</tr>
				</thead>
				<tbody>
					<%
					int entryPosition = 0;
					while (iteratorLibraries.hasNext()) {
						entryPosition++;
						Library library = iteratorLibraries.next();
					%>
					<tr id="rowId[<%=entryPosition%>]">
						<td><input hidden name="deleteRow[<%=entryPosition%>]"
							id="deleteRow[<%=entryPosition%>]"> <input hidden
							id="libraryName[<%=entryPosition%>]"
							name="libraryName[<%=entryPosition%>]"
							value="<%=library.getName()%>"> <input hidden
							name="externalId[<%=entryPosition%>]"
							value="<%=library.getExternalId()%>">
							<div id="libraryNameDisplay[<%=entryPosition%>]"><%=library.getName()%></div></td>
						<td><%=library.getExternalId()%></td>
						<td><button type="button" class="btn"
								id="renameLibrary[<%=entryPosition%>]"
								onclick="renameLibrary(<%=entryPosition%>)">Rename</button>
							<button type="button" class="btn" style="display: none;"
								id="saveRenameLibrary[<%=entryPosition%>]"
								onclick="saveRenameLibrary(<%=entryPosition%>)">Save</button>
							<button type="button" class="btn" style="display: none;"
								id="cancelRenameLibrary[<%=entryPosition%>]"
								onclick="cancelRenameLibrary(<%=entryPosition%>)">Cancel</button>
							<button type="button"
								onclick="document.getElementById('deleteRow[<%=entryPosition%>]').value = 'X';
								document.getElementById('rowId[<%=entryPosition%>]').style.display='none'"
								class="fa fa-trash icon-button"></button></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
			<button class="btn btn-success" name="addAllLibraries">Add
				all libraries</button>
		</form>
		<%
		}
		%>
		<form id="inputFileFormBooks" action="/LibrariesWebApp/LoadFromFile"
			method="POST">
			<div class="row">
				<div class="col-sm-3">
					<label for="filePathLibraries">Books:</label>
				</div>
				<div class="col-sm-3">
					<input type="file" id="filePathBooks" name="filePathBooks">
				</div>
				<div class="col-sm-6" style="text-align: left">
					<button class="btn">Load</button>
				</div>
			</div>
		</form>
		<form id="inputFileFormLibrariesBooks"
			action="/LibrariesWebApp/LoadFromFile" method="POST">
			<div class="row">
				<div class="col-sm-3">
					<label for="filePathLibrariesBooks">Books in Libraries:</label>
				</div>
				<div class="col-sm-3">
					<input type="file" id="filePathLibrariesBooks"
						name="filePathLibrariesBooks">
				</div>
				<div class="col-sm-6" style="text-align: left">
					<button class="btn">Load</button>
				</div>
			</div>
		</form>
		<%
		if (request.getAttribute("books") != null) {
			List<Book> books = (List<Book>) request.getAttribute("books");
			Iterator<Book> iteratorBooks = books.iterator();
		%>
		<form id="addAllBooks" action="/LibrariesWebApp/LoadFromFile"
			method="POST">
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Author</th>
						<th>Date Published</th>
						<th>External ID</th>
						<th />
					</tr>
				</thead>
				<tbody>
					<%
					int entryPosition = 0;
					while (iteratorBooks.hasNext()) {
						entryPosition++;
						Book book = iteratorBooks.next();
					%>
					<tr id="rowIdBook[<%=entryPosition%>]">
						<td><input hidden name="deleteRowBook[<%=entryPosition%>]"
							id="deleteRowBook[<%=entryPosition%>]"> <input hidden
							id="bookName[<%=entryPosition%>]"
							name="bookName[<%=entryPosition%>]" value="<%=book.getName()%>">
							<input hidden name="externalIdBook[<%=entryPosition%>]"
							value="<%=book.getExternalId()%>">
							<div id="bookNameDisplay[<%=entryPosition%>]"><%=book.getName()%></div>
						</td>
						<td><input hidden name="author[<%=entryPosition%>]"
							id="author[<%=entryPosition%>]" value="<%=book.getAuthor()%>">
							<div id="authorDisplay[<%=entryPosition%>]"><%=book.getAuthor()%></div></td>
						<td><input hidden name="datePublished[<%=entryPosition%>]"
							id="datePublished[<%=entryPosition%>]"
							value="<%=book.getDatePublished()%>" type="date">
							<div id="datePublishedDisplay[<%=entryPosition%>]"><%=book.getDatePublished()%></div></td>

						<td><%=book.getExternalId()%></td>
						<td><button type="button" class="btn"
								id="changeBook[<%=entryPosition%>]"
								onclick="changeBook(<%=entryPosition%>)">Change</button>
							<button type="button" class="btn" style="display: none;"
								id="saveBookChange[<%=entryPosition%>]"
								onclick="saveBookChange(<%=entryPosition%>)">Save</button>
							<button type="button" class="btn" style="display: none;"
								id="cancelChangeBook[<%=entryPosition%>]"
								onclick="cancelChangeBook(<%=entryPosition%>)">Cancel</button>
							<button type="button"
								onclick="document.getElementById('deleteRowBook[<%=entryPosition%>]').value = 'X';
								document.getElementById('rowIdBook[<%=entryPosition%>]').style.display='none'"
								class="fa fa-trash icon-button"></button></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
			<button class="btn btn-success" name="addAllBooks">Add all
				books</button>
		</form>
		<%
		}
		%>
		<%
		if (request.getAttribute("wrongFileType") != null) {
		%>
		<h1 style="color: red">
			The file type
			<%=request.getAttribute("wrongFileType")%>
			is not supported!
		</h1>
		<%
		}
		if (request.getAttribute("exceptions") != null) {
		List<String> errors = (List<String>) request.getAttribute("exceptions");
		Iterator<String> iteratorErrors = errors.iterator();
		while (iteratorErrors.hasNext()) {
		%>
		<h2 style="color: red">
			<%=iteratorErrors.next()%>
		</h2>
		<%
		}
		}
		%>
		<%
		if (request.getAttribute("successes") != null) {
			List<String> successes = (List<String>) request.getAttribute("successes");
			Iterator<String> iteratorErrors = successes.iterator();
			while (iteratorErrors.hasNext()) {
		%>
		<h2 style="color: green">
			<%=iteratorErrors.next()%>
		</h2>
		<%
		}
		}
		%>
		<%
		if (request.getAttribute("librariesBooks") != null) {
			List<BookLibrary> booksLibraries = (List<BookLibrary>) request.getAttribute("librariesBooks");
			Iterator<BookLibrary> iteratorBooksLibraries = booksLibraries.iterator();
		%>
		<form id="addBooksInLibraries" action="/LibrariesWebApp/LoadFromFile"
			method="POST">
			<table class="table">
				<thead>
					<tr>
						<th>Library</th>
						<th>Book</th>
						<th>External Entry Id</th>
					</tr>
				</thead>
				<tbody>
					<%
					int row = 0;
					while (iteratorBooksLibraries.hasNext()) {
						BookLibrary bookLibrary = iteratorBooksLibraries.next();
						row++;
					%>
					<tr id="row[<%=row%>]">
						<td><input hidden name="libraryId[<%=row%>]"
							value="<%=bookLibrary.getLibraryId()%>"> <%=bookLibrary.getLibraryName()%></td>
						<td><input hidden name="bookId[<%=row%>]"
							value="<%=bookLibrary.getBookId()%>"> <%=bookLibrary.getBookName()%></td>
						<td><input hidden name="externalId[<%=row%>]"
							value="<%=bookLibrary.getExternalId()%>"><%=bookLibrary.getExternalId()%>
							<input id="deleteRow[<%=row%>]" name="deleteRow[<%=row%>]" hidden>
							<button type="button"
								onclick="document.getElementById('deleteRow[<%=row%>]').value = 'X';
								document.getElementById('row[<%=row%>]').style.display='none'"
								class="fa fa-trash icon-button"></button></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
			<button class="btn btn-success" name="addBooksInLibraries">Add</button>
		</form>
		<%
		}
		%>
	</div>
	<script type="text/javascript">
	
	////Libraries
		function renameLibrary(position) {
			document.getElementById("renameLibrary[" + position + "]").style.display = "none";
			document.getElementById("libraryName[" + position + "]").style.display = "inline-block";
			document.getElementById("libraryNameDisplay[" + position + "]").style.display = "none";
			document.getElementById("saveRenameLibrary[" + position + "]").style.display = "inline-block";
			document.getElementById("cancelRenameLibrary[" + position + "]").style.display = "inline-block";
		}
		
		function saveRenameLibrary(position){
			revertInitialDisplayButtons(position);
			document.getElementById("libraryNameDisplay[" + position + "]").innerHTML = document.getElementById("libraryName[" + position + "]").value;
		}
		
		function revertInitialDisplayButtons(position){
			document.getElementById("renameLibrary[" + position + "]").style.display = "inline-block";
			document.getElementById("libraryName[" + position + "]").style.display = "none";
			document.getElementById("libraryNameDisplay[" + position + "]").style.display = "inline-block";
			document.getElementById("saveRenameLibrary[" + position + "]").style.display = "none";
			document.getElementById("cancelRenameLibrary[" + position + "]").style.display = "none";	
		}
		
        function cancelRenameLibrary(position){
            revertInitialDisplayButtons(position);
            document.getElementById("libraryName[" + position + "]").value = document.getElementById("libraryNameDisplay[" + position + "]").innerHTML;
		}
        
        //////Books
		function changeBook(position) {
			document.getElementById("changeBook[" + position + "]").style.display = "none";
			document.getElementById("bookName[" + position + "]").style.display = "inline-block";
			document.getElementById("bookNameDisplay[" + position + "]").style.display = "none";
		
			document.getElementById("author[" + position + "]").style.display = "inline-block";
			document.getElementById("authorDisplay[" + position + "]").style.display = "none";
			
			document.getElementById("datePublished[" + position + "]").style.display = "inline-block";
			document.getElementById("datePublishedDisplay[" + position + "]").style.display = "none";
			
			document.getElementById("saveBookChange[" + position + "]").style.display = "inline-block";
			document.getElementById("cancelChangeBook[" + position + "]").style.display = "inline-block";
	
        }
		
		function saveBookChange(position){
			revertInitialBooksDisplayButtons(position);
			document.getElementById("bookNameDisplay[" + position + "]").innerHTML = document.getElementById("bookName[" + position + "]").value;
			document.getElementById("authorDisplay[" + position + "]").innerHTML = document.getElementById("author[" + position + "]").value;
			document.getElementById("datePublishedDisplay[" + position + "]").innerHTML = document.getElementById("datePublished[" + position + "]").value;
		}
		
		function revertInitialBooksDisplayButtons(position){
			document.getElementById("changeBook[" + position + "]").style.display = "inline-block";
			document.getElementById("bookName[" + position + "]").style.display = "none";
			document.getElementById("author[" + position + "]").style.display = "none";
			document.getElementById("authorDisplay[" + position + "]").style.display = "inline-block";
			
			document.getElementById("datePublished[" + position + "]").style.display = "none";
			document.getElementById("datePublishedDisplay[" + position + "]").style.display = "inline-block";
			
			document.getElementById("bookNameDisplay[" + position + "]").style.display = "inline-block";
			document.getElementById("saveBookChange[" + position + "]").style.display = "none";
			document.getElementById("cancelChangeBook[" + position + "]").style.display = "none";	
		}
		
        function cancelChangeBook(position){
        	revertInitialBooksDisplayButtons(position);
            document.getElementById("bookName[" + position + "]").value = document.getElementById("bookNameDisplay[" + position + "]").innerHTML;
            document.getElementById("author[" + position + "]").value = document.getElementById("authorDisplay[" + position + "]").innerHTML;
			document.getElementById("datePublished[" + position + "]").value = document.getElementById("datePublishedDisplay[" + position + "]").innerHTML;
		
        }
	</script>
</body>