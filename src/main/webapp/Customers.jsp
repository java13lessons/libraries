<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Customer"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%
Map<Integer, Customer> allCustomers = (Map<Integer, Customer>) request.getAttribute("customersList");
if (allCustomers == null)
	allCustomers = new TreeMap<Integer, Customer>();
%>
<jsp:include page="header-template.jsp" />
<body>
	<div class="container">
		<form id="customerForm" action="/LibrariesWebApp/Customers"
			method="POST">
			<input hidden id="deleteCustomerId" name="deleteCustomerId">
			<input hidden id="customerRenamed" name="customerRenamed">
			<ul class="list-group" style="width: 50%; word-wrap: break-word">
				<%
				Iterator<Entry<Integer, Customer>> iterator = allCustomers.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Customer> customer = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<a id="displayCustomerNameSurname[<%=customer.getKey()%>]"
							href="/LibrariesWebApp/Customer?customerId=<%=customer.getKey()%>">
							<%=customer.getValue().getName()%> <%=customer.getValue().getSurname()%>

						</a> <input hidden id="renameName[<%=customer.getKey()%>]"
							name="renameName[<%=customer.getKey()%>]"
							value="<%=customer.getValue().getName()%>"> <input hidden
							id="renameSurname[<%=customer.getKey()%>]"
							name="renameSurname[<%=customer.getKey()%>]"
							value="<%=customer.getValue().getSurname()%>">
						<button type="button"
							onclick="document.getElementById('deleteCustomerId').value = <%=customer.getKey()%>;
						document.getElementById('customerForm').submit()"
							class="fa fa-trash icon-button"></button>
						<button type="button" id="renameButton[<%=customer.getKey()%>]"
							onclick="onRename(<%=customer.getKey()%>)" class="btn"
							style="text-align: right;">Rename</button>
						<button type="button" class="btn"
							id="saveCustomer[<%=customer.getKey()%>]" style="display: none"
							onclick="document.getElementById('customerRenamed').value = <%=customer.getKey()%>;
						document.getElementById('customerForm').submit()">Save</button>
						<button type="button" class="btn"
							onclick="onCancel(<%=customer.getKey()%>)"
							id="cancelInput[<%=customer.getKey()%>]" style="display: none">Cancel</button>
					</div>
				</li>
				<%
				}
				%>
			</ul>
			<div class="row">
				<label for="customerNameSurname">Customer name/surname</label> <input
					id="customerNameInsert" name="customerNameInsert"> <input
					id="customerSurnameInsert" name="customerSurnameInsert">
				<button name="insertNewCustomer" type="submit"
					class="btn btn-success">Insert</button>
			</div>
		</form>
		<%
		if (request.getAttribute("customerInserted") != null && (boolean) request.getAttribute("customerInserted") == true) {
		%>
		<div>
			<h1 style="color: green">Entry is entered!</h1>
		</div>
		<%
		}
		%>
	</div>
	<script>
	function onRename(key){
		document.getElementById("renameName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("renameSurname[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayCustomerNameSurname[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveCustomer[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("renameName[".concat(key).concat("]")).style.display='none';
		document.getElementById("renameSurname[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayCustomerNameSurname[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("saveCustomer[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>
</html>